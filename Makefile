all: morsegen

.PHONY: all clean check

clean:
	$(RM) morsegen result.yaml tr*.yarn
	$(RM) -r mustard
	$(RM) *~

mustard:
	git clone ssh://git@git.jlrngi.com/demo/mustard.git

check result.yaml: morsegen mustard morsegen-impl.yarn
	(cd mustard; git pull)
	$(RM) result.yaml
	loom -m mustard -O result.yaml -i morsegen-impl.yarn
